import binascii
pt = "In quantum information theory, superdense coding is a quantum communication protocol to transmit two classical bits of information (i.e., either 00, 01, 10 or 11) from a sender (often called Alice) to a receiver (often called Bob), by sending only one qubit from Alice to Bob, under the assumption of Alice and Bob pre-sharing an entangled state. X-MAS{3xtra_DEnS3_C0d1ng_GANG}"
gen = ""

state=0
#phi+ = 0 = 00
#phi- = 1 = 10
#psi+ = 2 = 01
#psi- = 3 = 11

ptbin = bin(int(binascii.hexlify(pt), 16))[2:]
if(len(ptbin) % 2 != 0):
    ptbin = "0"+ptbin
binarr = [ptbin[i:i+2] for i in range(0, len(ptbin), 2)]
print str(binarr)

for cur in binarr:
    if(state == 0): #00
        if(cur == "00"):
            gen += "I "
            state = 0
        elif(cur == "01"):
            gen += "X "
            state = 2
        elif(cur == "10"):
            gen += "Z "
            state = 1
        elif(cur == "11"):
            gen += "ZX "
            state = 3
        else:
            exit("Unknown cur: {}".format(cur))
    elif(state == 1): #10
        if(cur == "00"):
            gen += "Z "
            state = 0
        elif(cur == "01"):
            gen += "ZX "
            state = 2
        elif(cur == "10"):
            gen += "I "
            state = 1
        elif(cur == "11"):
            gen += "X "
            state = 3
        else:
            exit("Unknown cur: {}".format(cur))
    elif(state == 2): #01
        if(cur == "00"):
             gen += "X "
             state = 0
        elif(cur == "01"):
            gen += "I "
            state = 2
        elif(cur == "10"):
            gen += "ZX "
            state = 1
        elif(cur == "11"):
            gen += "Z "
            state = 3
        else:
            exit("Unknown cur: {}".format(cur))
    elif(state == 3): #11
        if(cur == "00"):
             gen += "ZX "
             state = 0
        elif(cur == "01"):
             gen += "Z "
             state = 2
        elif(cur == "10"):
             gen += "X "
             state = 1
        elif(cur == "11"):
            gen += "I "
            state = 3
        else:
            exit("Unknown cur: {}".format(cur))
    else:
        exit("Unknown state: {}".format(state))


print gen