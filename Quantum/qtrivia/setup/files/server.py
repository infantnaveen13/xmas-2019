FLAG = open('/chall/flag.txt', 'r').read().strip()

def question(q, a):
    print("Question: {}".format(q))

    for i in range(len(a)):
        a[i] = a[i].lower()

    while True:
        ans = input("Your answer: ").lower()

        if(ans in a):
            print("You got it right!")
            print("")
            break
        else:
            print("You got it wrong :(")

# General Knowledge
# Quantum Gates
# Superposition
# Entanglement

print("Welcome to Quantum Trivia! You will be presented with 4 categories with 5 questions each.")
print("If you answer them correctly, I will reward you with a flag! Hooray!")
print("NOTE: ONLY USE ASCII when answering questions. That means no foreign characters.")
print("Good luck!")
print("")

# =============== General Knowledge ===============
print("=============== General Knowledge ===============")
print("")
question("How are the special bits called in quantum computing?", ["qubits", "qubit"])
question("What is the name of the graphical representation of the qubit?", ["bloch", "blochsphere", "bloch sphere"])
question("In what state is a qubit which is neither 1 or 0?", ["superposition", "in superposition"])
question("What do you call 2 qubits which have interacted and are now in a wierd state in which they are correlated", ["entanglement", "entangled"])
question("What do you call the notation used in Quantum Computing where qubits are represented like this: |0> or |1>?", ["dirac", "bra-ket", "ket-bra", "braket", "ketbra", "diract notation"])
print("")

# =============== Quantum Gates ===============
print("=============== Quantum Gates ===============")
print("")
question("What gate would you use to put a qubit in a superposition?", ["h", "hadamard"])
question("What gate would you use to entangle 2 qubits?", ["CNOT", "Controlled NOT"])
question("What gate would you use to \"flip\" a qubit's phase in a superposition", ["Z", "pauli-z", "pauli z"])
question("What's the full name of the physicist who invented the X, Y and Z gates?", ["Wolfgang Pauli", "Pauli Wolfgang", "Wolfgang Ernst Pauli", "Pauli Ernst Wolfgang"])
question("What are quantum gates represented by (in dirac notation)?", ["matrix", "matrixes", "unitary", "unitary matrix", "unitary matrixes", "u matrix", "umatrix", "unitaries"])
print("")

# =============== Superposition ===============
print("=============== Superposition ===============")
print("")
question("How do you represent a qubit |1> put in a superposition (in dirac)?", ["|->"])
question("Will a superposition break if measured?", ["y", "yes", "yup", "yeah"])
question("Can you take a qubit out of a superposition with a Hadamard gate?", ["y", "yes", "yup", "yeah"])
question("If you measure a qubit in a superposition, what's the average chance of measruing |0>?", ["50", "50%", "50/50", "0.5"])
question("What's the name of the famous paradox which demonstrates the problem of decoherence?", ["Schrodinger's cat", "Schrodinger cat", "The Schrodinger's cat", "The Schrodinger cat", "Schrodingers cat", "The Schrodingers cat"])
print("")

# =============== Entanglement ===============
print("=============== Entanglement ===============")
print("")
question("Will particles always measure the same when entangled?", ["no", "n", "nah", "nope", "fuck no"])
question("Will entangled qubits violate Bell's Inequality?", ["y", "yes", "yup", "yeah"])
question("Does the following state present 2 entangled qubits? 1/sqrt(2)*(|10> + |11>)", ["no", "n", "nah", "nope"])
question("Does the following state present 2 entangled qubits? 1/sqrt(2)*(|10> + |01>)", ["y", "yes", "yup", "yeah"])
question("Can 2 entangled qubits ever get untangled?", ["y", "yes", "yup", "yeah"])
print("")

print("Congratz! You made it! ;)")
print("Here's your flag: " + FLAG)