<head>
	<link rel="stylesheet" type="text/css" href="styles.css">
</head>
<body>
	<div class="intro">
		<div class="center">
			<h1>Eggnog-Clinic Medical Record Database</h1>
			<img src = "/23c12189dcu91n8uc198231c9n412c4189dsa/img/eggnog.png"></img>
		</div>
	</div>

<!-- Database security check of this page is due 01/01/2020 -->

<form class="center">
	<label>Patient ID:</label>
	<input type="text" id="guess" name="id" maxlength="16" size="16" autocomplete="off">
	<input type="submit" value="Search">
</form>
<br>

<?php
$servername = "localhost";
$username = "HTsP";
$password = "HTsPAccessKey";

$conn = new mysqli($servername, $username, $password);
mysqli_select_db ($conn, "ctf");

if (isset($_GET['id'])) {
	$id = $_GET['id'];
	$records = mysqli_query($conn, "SELECT * FROM patients WHERE patient_id='" . $id . "'", MYSQLI_USE_RESULT);

	if ($records === false) {
		die ("<p style='text-align: center'>Our servers have run into a query error. Please try again later.</p>");
	}

	echo '<table>';
	echo '
	<tr>
		<th>Patient ID</th>
		<th>Name</th>
		<th>Surname</th>
		<th>Gender</th>
		<th>Address</th>
		<th>Race</th>
		<th>Birth Date</th>
		<th>Blood Type</th>
		<th>Body Weight</th>
		<th>Body Height</th>
		<th>Diagnosis</th>
	</tr>';

	while ($row = mysqli_fetch_array ($records, MYSQLI_ASSOC)) {
        printRow ($row);
    }

	echo '</table>';
}

function printRow ($patient) {
		echo '
	<tr>
		<td>',$patient["patient_id"],'</td>
		<td><b>',$patient["name"],'</b></td>
		<td><b>',$patient["surname"],'</b></td>
		<td>',$patient["gender"],'</td>
		<td>',$patient["address"],'</td>
		<td>',$patient["race"],'</td>
		<td>',$patient["birthdate"],'</td>
		<td>',$patient["bloodtype"],'</td>
		<td>',$patient["weight"],'</td>
		<td>',$patient["height"],'</td>
		<td>',$patient["diagnosis"],'</td>
	</tr>';
}
?>

</body>