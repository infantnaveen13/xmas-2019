<?php

session_start();
if(isset($_SESSION["code"]) && $_SESSION["code"] == "25548857362" && isset($_SESSION["logged"]) && $_SESSION['logged']){
	header("location: /panel");
	exit();
}

?>

<html>
<head>
<link href="https://fonts.googleapis.com/css?family=Raleway&display=swap" rel="stylesheet">
<link href="css/bootstrap.min.css" rel="stylesheet">
</head>
<body style="background-size: 100%; background-repeat: no-repeat;" background="bg.jpg">
<div style="position: absolute; height: 90%; width: 90%; color: #468da3">
<span style="position: absolute; font-family: 'Raleway', sans-serif; font-size: 2.7vw; right: 7%; top: 24%">Please Enter the Access Code</span>
<input id="accc" style="position: absolute; font-size: 2.4vw; right: 14%; top: 40%; background-color: rgba(255,255,255,0.2); text-align: center; width: 25%" type="text" class="form-control" placeholder="code" aria-label="code" aria-describedby="basic-addon1">
<button style="position: absolute; font-size: 1vw; right: 23.5%; top: 55%" onclick="sendCode()" type="button" class="btn btn-dark">Start Car</button>
</div>
</body>
<script src="js/bootstrap.min.js"></script>
<script src="main.js"></script>
</html>