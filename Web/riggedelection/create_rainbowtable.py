import random, hashlib, string

f = open ("rain", "w")
dct = {}
cnt = 0

while True:
    strn = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(random.randint (8, 16)))
    
    hash = hashlib.md5 (("watch__bisqwit__" + strn).encode ("ASCII")).hexdigest ()[:6]
    
    if (hash not in dct):
        dct[hash] = 1
        f.write (strn + ":" + hash + "\n")
        cnt += 1
        if (cnt % 100000 == 0):
            print ("Got {0} Hashes".format (cnt))