f = open ("rom.byte", "r").read ().split ("\n")
o = open ("rom", "wb")

bts = 0

for line in f:
	if (line.find (";") != -1):
		continue
		
	line = line.split (":")
	if (len (line) == 1):
		line = line[0]
	else:
		linenum = int (line [0], 16)

		while (linenum > bts):
			bts += 1
			o.write (chr (0))

		line = line[1][1:]

	line = line.split (" ")
	
	if (line [0] != ""):
		bts += len (line)
		for c in line:
			o.write (chr (int (c, 16)))

o.close ()
o = open ("rom", "rb").read ()[0x325:]
open ("rom","wb").write (o)