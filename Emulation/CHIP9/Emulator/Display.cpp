#include "Display.h"

inline u8 GetBit (u16 byte, u16 position) {
	return (byte & (1 << position)) != 0;
}

Display::Display (const char* title, u16 _width, u16 _height, u8 _pixelSize) {
	pixelSize = _pixelSize;
	width = _width;
	height = _height;

	mainWindow = SDL_CreateWindow (title, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width * pixelSize, height * pixelSize, SDL_WINDOW_SHOWN);
	mainRenderer = SDL_CreateRenderer (mainWindow, -1, SDL_RENDERER_ACCELERATED);
	mainTexture = SDL_CreateTexture (mainRenderer, SDL_PIXELFORMAT_RGB332, SDL_TEXTUREACCESS_STREAMING, width, height);
	Clear ();
}

void Display::Draw (i8 posX, i8 posY, u8 byte) {
	for (int i = 0; i < 8; i++) {
		if (posX + 7 - i >= 0 && posY >= 0 && posX < width && posY < height)
			SetPixel (posX + 7 - i, posY, GetBit (byte, i) ? Color:BGColor);
	}
}

void Display::Render () {
	SDL_UpdateTexture (mainTexture, NULL, pixels, width);
	SDL_RenderCopy (mainRenderer, mainTexture, NULL, NULL);
	SDL_RenderPresent (mainRenderer);
}

void Display::Clear () {
	memset (pixels, BGColor, sizeof (pixels));
}

inline void Display::SetPixel (u8 posX, u8 posY, u8 set) {
	pixels [posY * width + posX] = set;
}