from PIL import Image
import sys

img = Image.open ("flag.png")
rgb_im = img.convert('RGB')

xorkey = 0xEE
for i in range (23, 49):
    for k in range (0, 440, 8):
        strn = ""
        for bit in range (0, 8):
            #print (rgb_im.getpixel ((k + bit, i)))
            #print (rgb_im.getpixel ((k + bit, i)))
            if (rgb_im.getpixel ((k + bit, i)) == (255, 255, 255)):
                strn += "1"
            else:
                strn += "0"
        #sys.stdout.write (strn)
        sys.stdout.write (hex (int (strn, 2) ^ xorkey)[2:] + " ")
        xorkey += 0x6
        if (xorkey > 0xFF):
            xorkey -= 0x100
    sys.stdout.write ("\n")
    
sys.stdout.flush ()
