import socket
import string
from time import sleep

# START FUNCTIONS PART				
def read_buffer():
	global buff, pos

	buff = s.recv(1024)
	pos = 0				
	
def read_line():
	global buff, pos

	o = ''
	while buff[pos] != '\n':
		o += buff[pos]
		pos += 1
		if pos == len(buff):
			read_buffer()

	pos += 1
	if pos == len(buff):
		read_buffer()
	return o
	
def send(x):
	global queries
	
	s.sendall(x+'\n')
	queries += 1

def read_ans():
	read_line()
	ans = int(read_line().split()[-1])
	return ans
	
def search(preff, length, alphalen, alpha):
	global flag
	
	start = 0
	end = alphalen - 1
	while start < end:
		mid = (start + end) / 2

		if not preff + alpha[mid] + 'a' * (length - 1 - len(preff)) in d:
			print "Trying character: {}".format(alpha[mid])
			send(preff + alpha[mid] + 'a' * (length - 1 - len(preff)))
			ans1 = read_ans()
			if ans1 == 0:
				read_line()
				flag = read_line().split()[-1]
				return alpha[mid]
			d[preff + alpha[mid] + 'a' * (length - 1 - len(preff))] = ans1
		else: ans1 = d[preff + alpha[mid] + 'a' * (length - 1 - len(preff))]

		if not preff + alpha[mid + 1] + 'a' * (length - 1 - len(preff)) in d:
			print "Trying character: {}".format(alpha[mid + 1])
			send(preff + alpha[mid + 1] + 'a' * (length - 1 - len(preff)))
			ans2 = read_ans()
			if ans2 == 0:
				read_line()
				flag = read_line().split()[-1]
				return alpha[mid + 1]
			d[preff + alpha[mid + 1] + 'a' * (length - 1 - len(preff))] = ans2
		else: ans2 = d[preff + alpha[mid + 1] + 'a' * (length - 1 - len(preff))]
		
		if ans1 > ans2:
			start = mid + 1
		elif ans1 <= ans2:
			end = mid
	return alpha[start]
# END FUNCTIONS PART
	
#START ATTACK PART
#start connection
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect(('0.tcp.ngrok.io', 14981))

#initialize buffer
buff = ''
pos = 0
read_buffer()

#read intro
for i in range(40):
	print read_line()

#global declarations
d = {}
flag = ''
queries = 0
min = 2**100
low, high = 90, 100
String = ''
alpha = string.ascii_uppercase + string.ascii_lowercase
#guess String length
print "Guessing String length!\n"
while low < high:
	mid = (low + high) / 2
	
	if not 'a' * mid in d:
		print "Trying String length: {}".format(mid)
		send('a' * mid)
		ans1 = read_ans()
		d['a' * mid] = ans1
	else: ans1 = d['a' * mid]
	
	if not 'a' * (mid +1) in d:
		print "Trying String length: {}".format(mid + 1)
		send('a' * (mid + 1))
		ans2 = read_ans()
		d['a' * (mid + 1)] = ans2
	else: ans2 = d['a' * (mid + 1)]
	
	if ans1 > ans2:
		low = mid + 1
	else: high = mid
	
l = low	
print "String length recovered: {}".format(l)

#guess String byte by byte
print "\nGuessing String!\n"
for i in range(1, l+1):
	String += search(String, l, len(alpha), alpha)
	print String + 'a' * (l - i) + '\n'
	if flag != '':
		break

print "\nFound flag: {} in {} queries!".format(flag, queries)
#END ATTACK PART