from time import *
import random        
from hashlib import sha256
from binascii import *
from secret import FLAG
import sys
import os

def PoW():
        s = os.urandom(10)
        h = sha256(s).hexdigest()
        print("Provide a hex string X such that sha256(X)[-6:] = {}\n".format(h[-6:]))
        sys.stdout.flush()
        inp = raw_input()
        is_hex = 1
        for c in inp:
            if not c in '0123456789abcdef':
                is_hex = 0

        if is_hex and sha256(unhexlify(inp.encode())).hexdigest()[-6:] == h[-6:]:
            print('Good, you can continue!\n')
            sys.stdout.flush()
            return True
        else:
            print('Oops, your string didn\'t respect the criterion.\n')
            sys.stdout.flush()
            return False

def get_ans(i,q):
    vals = open("values{}.txt".format(str(i)),"r").read().split('\n')
    val = vals[q - 20 * 3**i]
    val = [int(a) for a in val.split(',')]
    return val

if not PoW():
        exit()

print("Hey there, Santa's distant relative here, Pythagora has a question for you!\nSit and listen, it might be an old story.")
sys.stdout.flush()
print("Note: the pythagorean triples are sorted increasingly by c (the hypotenuse), then by b (the long leg) then by a (the short leg).\nAlso you have 5 seconds / test.")
sys.stdout.flush()

error = False
last = 0

for round_idx in range(1, 11):
    print("Here's the challange #{}:".format(round_idx))
    sys.stdout.flush()
    query = random.randint(20 * 3**round_idx, 20 * 3**round_idx + 10**4)
    ans = get_ans(round_idx,query)
    print("Give me the {}-th primitive pythagorean triple in the following format: a,b,c with a < b < c.".format(query))
    sys.stdout.flush()
    start = time()
    inp = raw_input()
    if time()-start > 15:
        print("Time limit exceded. Aborting.")
        sys.stdout.flush()
        error = True
        break
    try:
        l=list(int(a) for a in inp.split(','))
        assert len(l) == 3
    except:
        print("Invalid input. Aborting!")
        sys.stdout.flush()
        error = True
        break
    if l != ans:
        print("Wrong Answer, the answer should've been {}. Aborting!".format(','.join([str(w) for w in ans])))
        sys.stdout.flush()
        error = True
        break
    print("Well done, here, have another.")
    sys.stdout.flush()

if error:
        exit()

print("Good one mate, Pythagora would be proud!\nHere's your flag: {}".format(FLAG))
sys.stdout.flush()
