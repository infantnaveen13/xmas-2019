import Augmentor
import os

SAMPLE_SIZE = 100

def create_sample(img_name):
        cwd = os.getcwd()
        try:
                os.chdir(cwd + '/Symbols/{}/output'.format(img_name))
                os.system('rm *')
                os.chdir(cwd)
        except:
                pass

        p = Augmentor.Pipeline('./Symbols/{}'.format(img_name))
        p.random_distortion(probability=1, grid_width=3, grid_height=3, magnitude=6)
        p.sample(SAMPLE_SIZE)

        os.chdir(cwd + '/Symbols/{}/output'.format(img_name))
        
        images = os.listdir()

        for i, image in enumerate(images):
                os.system("mv {} {}.bmp".format(image, str(i)))

        os.chdir(cwd)

for i in range(6):
        create_sample(str(i))
